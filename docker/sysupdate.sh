#!/bin/sh
# Update a base Slackware installation so it can be used for building packages
# Copyright (C) 2019-2021 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. */

set -e

. /etc/os-release

if [ "$VERSION_CODENAME" = "current" ] || [ "$VERSION" = 15.0 ]; then
    echo "Installing coreutils 8.25 to work around a docker bug"
    cd /tmp
    wget http://pirx.gnu.org.ua/slackware/slackware64-14.2/slackware64/a/coreutils-8.25-x86_64-2.txz
    installpkg ./coreutils-8.25-x86_64-2.txz
    removepkg $(find /var/log/packages/ -name 'coreutils*' -not -name coreutils-8.25-x86_64-2)
    cd /usr/bin
    ln -sf mktemp-gnu mktemp
    cd /
fi

# Update a list of available packages in /var/lib/slackpkg/pkglist
yes | slackpkg update

# Create a list of installed packages.  Each line in the output consists of
# three fields:
#  1. Package name
#  2. Package version
#  3. Build number
# Only the first field is actually used.  Rest is for future use.
find /var/log/packages -type f -maxdepth 1 -printf '%f\n' | \
  sed -r -n -e 's/(.*)-([[:digit:].]+.*)-x86_64-([0-9]+).*/\1 \2 \3/p' | \
  sort -k1 -k2V > /tmp/installed

# Create a list of available packages in series:
#   a   - base
#   ap  - applications
#   d   - development
#   e   - emacs
#   l   - libraries
#   n   - network
#   t   - TeX
#   tcl - Tcl/Tk and related
#   x   - X Window System
# Each output line has four fields.  The first three are the same as described
# above.  The fourth contains base name of the package without the archive
# suffix (.t[gx]z).
awk '/^slackware64/ {
     if (match($7, /\/[adelntx]$/) || match($7, /\/ap/) || match($7, /\/tcl/))
        print $2,$3,$5,$6}' \
    /var/lib/slackpkg/pkglist | \
  sort -k1 -k2V > /tmp/available

# Install missing packages
slackpkg install $(join -v 2 /tmp/installed /tmp/available | awk '{print $4}')

# Clean up
rm -rf /tmp/*


