# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Counter;
use Test;

plan tests => 13;
    
my $c = new SlackBuild::Counter;

# 1
ok($c->categories(),0);

# 2
$c->more('errors');
$c->more('warnings');
$c->more('messages');
$c->incr('messages');
$c += 'messages';

ok(scalar($c->categories()), 3);

# 3
ok(join(',', sort $c->categories),'errors,messages,warnings');

# 4
ok($c->total, 5);

# 5
ok(scalar($c), 5);

# 6
ok($c == 5);

# 7
ok($c->get('messages'),3);

# 8
ok($c->messages,3);

# 9
ok($c->get('panic'),undef);

# 10
ok((eval { $c->panic }, $@), '/no such category/' );

# 11
$c->clear('messages','warnings');
ok(join(',',$c->categories()),'errors');
# 12
ok($c->total, 1);

# 13
$c->clear;
ok($c->total, 0);



