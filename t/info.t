# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Info;
use File::Temp;
use Test;

plan tests => 2;

my $infostr = <<'EOT';
PRGNAM="rdesktop"
VERSION="1.4.1"
HOMEPAGE="http://rdesktop.org"
DOWNLOAD="http://downloads.sourceforge.net/rdesktop/rdesktop-1.4.1.tar.gz"
MD5SUM="78dd2bae04edf1cb9f65c29930dcc993"
DOWNLOAD_x86_64=""
MD5SUM_x86_64=""
REQUIRES=""
MAINTAINER="Robby Workman"
EMAIL="rworkman@slackbuilds.org"
EOT
;
my $expect = q{{"DOWNLOAD":["http://downloads.sourceforge.net/rdesktop/rdesktop-1.4.1.tar.gz"],"DOWNLOAD_x86_64":null,"EMAIL":"rworkman@slackbuilds.org","HOMEPAGE":"http://rdesktop.org","MAINTAINER":"Robby Workman","MD5SUM":["78dd2bae04edf1cb9f65c29930dcc993"],"MD5SUM_x86_64":null,"PRGNAM":"rdesktop","REQUIRES":null,"VERSION":"1.4.1"}};

my $info = new SlackBuild::Info(\$infostr);
ok("$info", $expect);

my $fh = new File::Temp(UNLINK => 1);
print $fh $infostr;
$fh->flush;
$info = new SlackBuild::Info($fh->filename);
ok("$info", $expect);


