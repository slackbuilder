# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Registry::Version;
use Test;

plan tests => 8;

ok(SlackBuild::Registry::Version->new('1.0') ==
   SlackBuild::Registry::Version->new('1.0'));

ok(SlackBuild::Registry::Version->new('1.0') <
   SlackBuild::Registry::Version->new('1.1'));

ok(SlackBuild::Registry::Version->new('1.0.1') >
   SlackBuild::Registry::Version->new('1.0.0'));

ok(SlackBuild::Registry::Version->new('1.0.1') >
   SlackBuild::Registry::Version->new('1.0'));

ok(SlackBuild::Registry::Version->new('2.1,6') <
   SlackBuild::Registry::Version->new('2.2'));

ok(SlackBuild::Registry::Version->new('1.0_A') <
   SlackBuild::Registry::Version->new('1.0_B'));

ok(SlackBuild::Registry::Version->new('1.3_Z') <
   SlackBuild::Registry::Version->new('1.4_A'));

ok(SlackBuild::Registry::Version->new('1.0') == '1.0');

