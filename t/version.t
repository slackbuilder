# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Registry::Version;
use Test;

plan tests => 14;

my $v = new SlackBuild::Registry::Version('4');
ok($v->major, '4');
ok(!$v->minor);
ok(!$v->patch);

$v = new SlackBuild::Registry::Version('4.1.9');
ok($v->major, '4');
ok($v->minor, '1');
ok($v->patch, '9');

$v = new SlackBuild::Registry::Version('4.1.9m');
ok($v->major, '4');
ok($v->minor, '1');
ok($v->patch, '9');
ok($v->tail, 'm');

$v = new SlackBuild::Registry::Version('1.0.0-rc8');
ok($v->major, '1');
ok($v->minor, '0');
ok($v->patch, '0');
ok($v->tail, '-rc8');

