# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Registry::Record;
use SlackBuild::Registry::Pattern;
use Test;

plan tests => 12;

my $r = new SlackBuild::Registry::Record('foo',
	                                 arch => 'x86_64',
					 version => '1.0',
					 build => 1,
					 date => 0);

ok("$r", 'foo-1.0-x86_64-1');

my $x = new SlackBuild::Registry::Record('foo',
	                                 arch => 'x86_64',
					 version => '1.0',
					 build => 2,
					 date => 0);

ok($r < $x);
ok($r <=> $x, -1);

ok(SlackBuild::Registry::Pattern->new(version => { -gt => '0.2' })->matches($r));
ok(SlackBuild::Registry::Pattern->new(version => { -gt => '0.2' },
 	                              arch => { -in => [ qw(i386 x86_64)] })->matches($r));
ok(!SlackBuild::Registry::Pattern->new(version => { -gt => '0.2' },
 		                       arch => { -eq => 'i386' })->matches($r));

my $pat = new SlackBuild::Registry::Pattern(
             arch => { -in => [qw(noarch x86_64)] },
             build => 2,
             package => "bar",
    version => "1.1");

ok($pat->as_string,
   q{[{"package":{"-eq":"bar"}},{"version":{"-eq":"1.1"}},{"arch":{"-in":["noarch","x86_64"]}},{"build":{"-eq":2}}]});

$r = SlackBuild::Registry::Record->split('foo-1.0-x86_64-2');
ok($r);
ok($r->package, 'foo');
ok($r->version, '1.0');
ok($r->arch, 'x86_64');
ok($r->build, 2);
    
