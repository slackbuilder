# -*- perl -*-
use lib qw(t lib);
use strict;
use warnings;
use SlackBuild::Request;
use SlackBuild::Info;
use File::Temp;
use Test;

plan tests => 7;

#1
my $req = new SlackBuild::Request(
    package => 'foo',
    version => '1.0',
    slackbuild_uri => 'foo.tar.gz',
    source_uri => 'foo-1.0.tar.gz',
    prereq => [qw(bar baz)],
);

ok("$req",
   q{{"package":"foo","prereq":["bar","baz"],"slackbuild_uri":"foo.tar.gz","source_uri":["foo-1.0.tar.gz"],"version":"1.0"}});

#2
$req = new SlackBuild::Request(slackbuild_uri => 'foo.tar.gz');

my $infostr = <<'EOT';
PRGNAM="foo"
VERSION="1.1"
DOWNLOAD="foo-1.1.tar.gz bar-1.0.tar.gz"
EOT
;

$req->addinfo(new SlackBuild::Info(\$infostr));
ok("$req",
   q{{"package":"foo","slackbuild_uri":"foo.tar.gz","source_uri":["foo-1.1.tar.gz","bar-1.0.tar.gz"],"version":"1.1"}});

#3
$req = new SlackBuild::Request(
    package => 'foo',
    version => '1.0',
    slackbuild_uri => 'foo.tar.gz',
    source_uri => 'foo-1.0.tar.gz',
    prereq => [qw(bar baz)]
);

$infostr = <<'EOT';
PRGNAM="foo11"
VERSION="1.1"
DOWNLOAD="foo-1.1.tar.gz"
REQUIRES="%README% quux quuZ"  
EOT
;

$req->addinfo(new SlackBuild::Info(\$infostr));

ok("$req",
   q{{"package":"foo","prereq":["bar","baz","quux","quuZ"],"slackbuild_uri":"foo.tar.gz","source_uri":["foo-1.0.tar.gz"],"version":"1.0"}});

#4
$req = new SlackBuild::Request(
    package => 'foo',
    version => '1.0',
    slackbuild_uri => 'foo.tar.gz',
    source_uri => 'foo-1.0.tar.gz',
    prereq => [qw(bar baz)],
    strategy => { prereq => 'overwrite' }
);

$req->addinfo(new SlackBuild::Info(\$infostr));

ok("$req",
   q{{"package":"foo","prereq":["quux","quuZ"],"slackbuild_uri":"foo.tar.gz","source_uri":["foo-1.0.tar.gz"],"strategy":{"prereq":"overwrite"},"version":"1.0"}});

#5
$req = new SlackBuild::Request(
    package => 'foo',
    version => '1.0',
    slackbuild_uri => 'foo.tar.gz',
    source_uri => 'foo-1.0.tar.gz',
    prereq => [qw(bar baz)],
    strategy => { prereq => 'keep' }
);

$req->addinfo(new SlackBuild::Info(\$infostr));

ok("$req",
   q{{"package":"foo","prereq":["bar","baz"],"slackbuild_uri":"foo.tar.gz","source_uri":["foo-1.0.tar.gz"],"strategy":{"prereq":"keep"},"version":"1.0"}});

#6
{
    my $fh = new File::Temp(UNLINK => 1);
    print $fh <<'EOT'
{
    "package": "foo",
    "version": "1.0",
    "source_uri": ["foo-1.1.tar.gz","bar-1.0.tar.gz"],
    "build":"2",
    "prereq": "quux"
}
EOT
    ;
    $fh->flush;
    $req = load SlackBuild::Request($fh->filename);
    ok("$req",
       q{{"build":"2","package":"foo","prereq":["quux"],"source_uri":["foo-1.1.tar.gz","bar-1.0.tar.gz"],"version":"1.0"}});
}
#7
{
    my $fh = new File::Temp(UNLINK => 1);
    print $fh <<'EOT'
---
package: foo
version: 1.0
source_uri:
  - foo-1.1.tar.gz
  - bar-1.0.tar.gz
build: 2
prereq: quux
EOT
;
    $fh->flush;
    $req = load SlackBuild::Request($fh->filename);
    ok("$req",
       q{{"build":"2","package":"foo","prereq":["quux"],"source_uri":["foo-1.1.tar.gz","bar-1.0.tar.gz"],"version":"1.0"}});
}
