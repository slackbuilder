# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package Net::SBo;
use strict;
use warnings;
use POSIX qw(:sys_wait_h strerror);
use Carp;
use File::Basename;
use File::Spec;
use File::Path qw(make_path);
use Archive::Tar;

our $VERSION = '1.00';

=head1 NAME

Net::SBo - Access slackbuilds.org repository

=head1 SYNOPSIS

    use Net::SBo;

    $sbo = new Net::SBo;
    $sbo = new Net::SBo(dir => $cachedir);

    $path = $sbo->find('mailutils');
    ($path,$commit) = $sbo->find('mailutils', 'HEAD^');

    $sbo->scan('HEAD^');
    @files = $sbo->get($path, $dest, %opts);

    $x = $sbo->commit;

=head1 DESCRIPTION

B<Net::SBo> provides access to the L<slackbuilds.org> repository, allowing
the user to download slackbuild archives for arbitrary packages.

=head1 Object Methods
    
=head2 Net::SBo->new(%opts)

Returns a new B<SBo> object. A bare mirror of the slackbuilds repository is
created in the directory B<$HOME/.sbo.git>, unless it already exists, in
which case it is synchronized with the master repository. Optional keyword
arguments alter the default settings:

=over 4

=item B<dir>

Name of the local slackbuilds mirror to use.

=item B<repo>

URL of the slackbuilds repository. Default is
L<git://git.slackbuilds.org/slackbuilds.git>.

=back

If B<dir> is not given, the path to the repository is read from the
environment variable B<SBO_LOCAL_REPOSITORY>. If it is not set, it
defaults to B<.sbo.git> in the user home directory.
    
=cut

my $default_repo = 'git://git.slackbuilds.org/slackbuilds.git';

sub new {
    my ($class, %args) = @_;
    my $dir = delete $args{dir}
                 || $ENV{SBO_LOCAL_REPOSITORY}
                 || File::Spec->catfile($ENV{HOME}, '.sbo.git');
    my $repo = delete $args{repo} || $default_repo;
    
    my $self = bless {}, $class;
    my @cmd;
    if (-d $dir) {
	@cmd = (qw(git --git-dir), $dir, 'fetch', '-q')
    } else {
	@cmd = (qw(git clone -q --mirror), $repo, $dir);
    }
    if (@cmd) {
	if (system(@cmd)) {
	    $self->procdiag($?, @cmd);
	}
    }
    $self->{dir} = $dir;
    $self->{repo} = $repo;

    return $self;
}

sub procdiag {
    my ($self, $status) = (shift, shift);
    if (WIFEXITED($status)) {
	return if $status == 0;
	croak "command \""
	      . join(' ', @_)
	      . "\" terminated with code "
	      . WEXITSTATUS($status);
    } elsif (WIFSIGNALED($status)) {
	croak "command \""
	      . join(' ', @_)
	      . "\" terminated on signal "
	      . WTERMSIG($status);
    } else {
	croak "command \""
	      . join(' ', @_)
	      . "\" terminated with unrecogized code "
	      . $status;
    }
}

=head2 $sbo->scan([$treeish])

Scan the subtree. If I<$treeish> is not given, B<HEAD> is assumed.

Normally, you won't need to use this method, as it is called internally
when needed.    
    
=cut    

sub scan {
    my ($self, $treeish) = @_;
    $treeish //= 'HEAD';
    return if (exists($self->{treeish}) && $self->{treeish} eq 'HEAD');
    delete $self->{index};
    delete $self->{treeish};
    delete $self->{commit};
    
    my @cmd = ('git', '--git-dir', $self->{dir},
	       qw(ls-tree --full-tree -r --name-only),
	       $treeish);
    open(my $fh, '-|', @cmd)
	or croak "git ls-tree failed: $!";
    while (<$fh>) {
	chomp;
	my ($filename, $path, $suffix) = fileparse($_, '.SlackBuild');
	if ($suffix eq '.SlackBuild') {
	    $self->{index}{$filename} = $path;
	}
    }
    $self->{treeish} = $treeish;
    # FIXME: Use 'git --git-dir ~/.sbo.git rev-parse $treeish' to convert it
    # to the SHA.
}

=head2 $sbo->commit()

Returns the SHA-1 hash which identifies the scanned commit.

=cut    

sub commit {
    my ($self) = @_;
    unless ($self->{commit}) {
	$self->scan;
	my @cmd = ('git', '--git-dir', $self->{dir}, 'rev-parse',
		   $self->{treeish});
    
	open(my $fh, '-|', @cmd) or croak "git rev-parse failed: $!";
	chomp(my @a = <$fh>);
	$self->{commit} = $a[0];
    }
    return $self->{commit};
}

=head2 $sbo->find($package, [$treeish])

Looks up given package name in the tree. I<$treeish> defaults to B<HEAD>.
In list context returns B<(I<path>, I<commit>)>, where I<path> is the path
to the package file in the repository, and I<commit> is the commit id
corresponding to I<$treeish>. In scalar context, returns path.

If no such package exists, returns empty list in list context and B<undef> in
scalar context.    
    
=cut    

sub find {
    my ($self, $name, $treeish) = @_;
    $self->scan($treeish);
    if (wantarray) {
	if (exists($self->{index}{$name})) {
	    return ($self->{index}{$name}, $self->commit);
	} else {
	    return ();
	}
    }
    return $self->{index}{$name}
}

sub dir_is_empty {
    my ($self, $dir) = @_;
    opendir(my $dfh, $dir)
	or croak "can't open $dir: $!";
    my $res = 1;
    while (my $file = readdir $dfh) {
	next if $file eq '.' or $file eq '..';
	$res = 0;
	last;
    }
    closedir $dfh;
    return $res;
}

=head2 $sbo->get($path, $dest, %opts)

Retrieves files from I<$path>. By default files are enclosed in a tar archive
which is written to B<$dest>. Options are:

=over 4

=item B<treeish>

Specifies the identifier of the tree to retrieve the files from. Defaults to
B<HEAD>.

=item B<extract>

If true, the files are extracted from the archive into directory B<$dest>.

=back

Returns names of the retrieved files in list context, and their number in
scalar context.    

=cut    

sub get {
    my ($self, $path, $dest, %args) = @_;

    croak "bad number of arguments" unless defined $dest;
    
    my $treeish = delete $args{treeish} // 'HEAD';
    my $extract = delete $args{extract};
    croak "extra arguments" if keys(%args);
    
    if ($extract) {
	if (-d $dest) {
	    croak "$dest is not empty" unless $self->dir_is_empty($dest);
	} else {
	    croak "$dest exists, but is not a directory" if -e $dest;
	    make_path($dest, {error => \my $err});
	    croak @$err if (@$err);
	}
    }
    
    my @gitcmd = ( 'git', '--git-dir', $self->{dir}, 'archive',
		   $treeish, $path );
    open(my $fd, '-|', @gitcmd)	or croak "can't run git: $!";

    $path .= '/' unless $path =~ m{/$};

    my $tar = new Archive::Tar($fd);
    my @result;
    if ($extract) {
	$dest .= '/' unless $dest =~ m{/$};
	
	foreach my $file ($tar->get_files) {
	    my $name = $file->full_path;
	    if ($name =~ s{^\Q$path\E}{$dest}) {
		if ($file->extract($name)) {
		    push @result, $name;
		}
	    }
	}
    } else {
	for (my $comp = $path; $comp ne '.'; $comp = dirname($comp)) {
	    $tar->remove($comp);
	}
	foreach my $file ($tar->get_files) {
	    my $name = $file->full_path;
	    if ($name =~ s{^\Q$path\E}{}) {
		$file->rename($name);
	    }
	    push @result, $name;
	}
    }
    close $fd;
    $self->procdiag($?, @gitcmd);
    unless ($extract) {
	$tar->write($dest);
    }
    return @result;
}

1;
