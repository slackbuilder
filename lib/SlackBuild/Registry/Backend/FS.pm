# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Registry::Backend::FS;
use strict;
use warnings;
use Carp;
use File::Basename;
use File::stat;
use File::Spec;
use File::Path qw(make_path);
use Fcntl ':mode';
use SlackBuild::Registry::Record;

=head1 NAME
    
SlackBuild::Registry::Backend::FS - filesystem backend for slackbuild registry

=head1 SYNOPSIS

    $reg = new SlackBuild::Registry('FS', dir => DIRECTORY);
    $pat = new SlackBuild::Registry::Pattern(package => 'openssl',
                                             version => '1.0.2m');
    @a = $x->lookup($pat);

=head1 METHODS

=head2 new

    $x = new SlackBuild::Registry::Backend::FS(dir => DIRECTORY)
    
=cut
    
sub new {
    my $class = shift;
    my $self = bless {}, $class;
    local %_ = @_;
    if (my $dir = delete $_{dir}) {
	$self->{dir} = $dir;
    } else {
	croak "required parameter dir not present";
    }
    croak "too many arguments" if keys %_;
    if (! -d $self->{dir}) {
	make_path($self->{dir}, {error => \my $err});
	croak @$err if (@$err);
    }
    $self->scan;
    return $self;
}

my @suffixes = qw(.tgz .txz);

sub scan {
    my $self = shift;
    my $pat = "*-*-*-*";

    $self->{ls} = [sort {
	my $d;
	if ($d = ($a->package || '') cmp ($b->package || '')) {
	    $d
	} elsif ($d = $b->version <=> $a->version) {
	    $d
	} elsif ($a->arch && $b->arch
		 && ($d = $a->arch cmp $b->arch)) {
	    $d
        } else {
	    ($b->build || 1) <=> ($a->build || 1)
        }
    }
    map {
	my $rec = SlackBuild::Registry::Record->split($_);
	if ($rec) {
	    my $st = stat($_);
	    if (S_ISREG($st->mode)) {
		$rec->date($st->mtime);
		$rec->filename(File::Spec->abs2rel($_, $self->{dir}));
	    } else {
		$rec = undef;
	    }
	}
	$rec
    } (glob File::Spec->catfile($self->{dir}, $pat))];

    return unless @{$self->{ls}};
    
    $self->{index}{$self->{ls}[0]->package}[0] = 0;
    my $i;
    for ($i = 1; $i < @{$self->{ls}}; $i++) {
	unless ($self->{ls}[$i]->package eq $self->{ls}[$i-1]->package) {
	    $self->{index}{$self->{ls}[$i-1]->package}[1] = $i-1;
	    $self->{index}{$self->{ls}[$i]->package}[0] = $i;
	}
    }
    $self->{index}{$self->{ls}[$i-1]->package}[1] = $i-1;
}

=head2 lookup

    @a = $backend->lookup($pattern)

Returns a sorted array of SlackBuild::Registry::Record objects matching the
B<SlackBuild::Registry::Pattern> object B<$pattern>.
    
=cut
    
sub lookup {
    my ($self, $pred) = @_;
    my $pkg = $pred->package;

    my @result;
    if ($pkg) {
	if (my $idx = $self->{index}{$pkg}) {
	    @result = grep { $pred->matches($_) }
	    		@{$self->{ls}}[$idx->[0] .. $idx->[1]];
	}
    } else {
	@result = grep { $pred->matches($_) } @{$self->{ls}};
    }

    if (wantarray) {
	(@result)
    } else {
	shift @result;
    }
}

sub getlist {
    my ($self) = @_;
    return $self->{ls};
}

1;
