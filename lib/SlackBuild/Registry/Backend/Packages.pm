package SlackBuild::Registry::Backend::Packages;
use strict;
use warnings;
use Carp;
use SlackBuild::Registry::Record;
use File::Basename;
use File::Spec;

=head1 NAME

SlackBuild::Registry::Backend::Packages - PACKAGES.TXT backend for slackbuild

=head1 SYNOPSIS

    $reg = new SlackBuild::Registry('Packages', dir => DIRECTORY);
  or
    $reg = new SlackBuild::Registry('Packages', file => FILENAME);

=head1 DESCRIPTION

This module parses Slackware PACKAGES.TXT file and serves the collected
information as SlackBuild package registry.    

=head1 CONSTRUCTOR

The constructor can be called either with C<dir> parameter or with
C<file>, but not with both.  When called with C<dir> (first example
in the B<SYNOPSIS> section), it reads the file F<I<DIRECTORY>/PACKAGES.TXT>.
When called with C<file>, it reads the named file.    

=cut    

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    local %_ = @_;

    if (my $filename = delete $_{file}) {
	$self->{file} = $filename;
	$self->{dir} = dirname($filename);
    }

    if (my $dir = delete $_{dir}) {
	if (exists($self->{filename})) {
	    croak "either dir or file must be specified, not both";
	}
	$self->{dir} = $dir;
	$self->{file} = File::Spec->catfile($dir, 'PACKAGES.TXT');
    }
    
    unless (exists($self->{dir})) {
	croak "either dir or file must be specified";
    }
    croak "too many arguments" if keys %_;
    $self->scan;
    return $self;
}

sub scan {
    my $self = shift;

    if (open(my $fd, '<', $self->{file})) {
	$self->{state} = \&_state_initial;
	while (<$fd>) {
	    chomp;
	    $self->${ \$self->{state} }($_);
	}
	close $fd;
    } else {
	croak "can't open $self->{file}: $!";
    }
}

sub _state_initial {
    my ($self, $text) = @_;
    if ($text =~ /^PACKAGE NAME:\s+(.+)$/) {
	$self->{record} = SlackBuild::Registry::Record->split($1);
	push @{$self->{list}}, $self->{record};
	$self->{state} = \&_state_package;
    }
}

sub _state_package {
    my ($self, $text) = @_;
    if ($text =~ /^PACKAGE (.+?):\s*(.*)$/) {
	my ($kw, $val) = ($1, $2);
	if ($kw eq 'DESCRIPTION') {
	    $self->{state} = \&_state_description_0;
	    return;
	} else {
	    $kw =~ s/([A-Z]+)\s+\(((?:un)?compressed)\)/\L$1_$2\E/;
	    if ($self->{record}->can($kw)) {
		if ($kw =~ /REQUIRED|CONFLICTS|SUGGESTS/) {
		    $self->{record}->${\$kw}(parse_line(',', 0, $val))
		} else {
		    $self->{record}->${\$kw}($val);
	        }
	    }
	    # else: Ignore unrecognized keywords
	}
    } else {
	$self->{state} = \&_state_ignore;
	goto &_state_ignore;
    }
}

sub _state_description {
    my ($self, $text, %args) = @_;
    my $pkgname = $self->{record}->package;
    
    if ($text eq '') {
	$self->{state} = \&_state_initial;
	return;
    }

    if ($text =~ s/\Q$pkgname\E:\s*//) {
	$self->{record}->description(append => $text, %args);
    } else {
	$self->{state} = \&_state_initial;
	goto &_state_initial;
    }
}	

sub _state_description_0 {
    my $self = shift;
    $self->_state_description(@_, newline => 0);
    $self->{state} = \&_state_description
	unless $self->{state} == \&_state_initial;
}

sub _state_ignore {}

sub lookup {
    my ($self, $pred) = @_;

    my @result = grep { $pred->matches($_) } @{$self->{list}};

    if (wantarray) {
	(@result)
    } else {
	shift @result;
    }
}

sub getlist {
    my ($self) = @_;
    return $self->{list};
}

1;
