# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Registry::Record;
use strict;
use warnings;
use Carp;
use SlackBuild::Base qw(package arch build date filename tag suffix vpref
                        md5sum priority location
                        size_compressed size_uncompressed
                        required:array conflicts:array suggests:array
                        uri);
use SlackBuild::Registry::Version;
use List::Regexp;
use File::Basename;

=head2 new

    $x = new SlackBuild::Registry::Record(PACKAGE,
                     arch=>X, version=>Y, build=>Z, date=>D, filename=>F)
    
=cut

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    $self->build(1);
    if (my $v = shift) {
	$self->package($v);
	if (@_) {
	    croak "bad number of arguments" if (@_ % 2);
	    local %_ = @_;
	    while (my ($k,$v) = each %_) {
		$self->${\$k}($v);
	    }
	}
    }
    return $self;
}

my @architectures = qw(i386 x86_64 arm noarch);

my $rx = '^(?<pkg>.+)-'
           . '(?<vpref>[rvV]?)'
	   . '(?<version>(?:hg)?\d+(?:\.\d+)*.*?)'
	   . '-(?<arch>' . regexp_opt(@architectures) . ')'
	   . '-(?<build>\d+)'
           . '(?:_(?<tag>.+?))?$';

=head2 split

    $r = SlackBuild::Registry::Record->split($filename, %opt)

=cut    

my @suffixes = qw(.tgz .txz .tbz .tlz);

sub split {
    my ($class, $pkgfilename, %opt) = @_;
    my ($filename,$path,$suffix) = fileparse($pkgfilename, @suffixes);
    if ($filename =~ m{$rx}) {
	return $class->new($+{pkg},
			   version => $+{version},
			   arch => $+{arch},
			   build => $+{build},
			   vpref => $+{vpref},
			   tag => $+{tag},
			   suffix => $+{suffix},
			   filename => $filename,
			   suffix => $suffix,
			   uri => $pkgfilename,#FIXME
			   %opt);
    }
}

sub version {
    my $self = shift;
    if (@_) {
	croak "too many arguments" if @_ > 1;
	$self->{version} = new SlackBuild::Registry::Version(shift);
    }
    return $self->{version};
}

sub description {
    my $self = shift;
    if (@_ == 1) {
	$self->{description} = shift;
    } elsif (@_ % 2 == 0) {
	local %_ = @_;
	if (defined(my $append = delete $_{append})) {
	    if ($_{newline}//1) {
		$append = "\n".$append;
	    }
	    $self->{description} .= $append;
	} else {
	    croak "append not given";
	}
    } else {
	croak "bad arguments";
    }
    return $self->{description}
}

sub store {
    my $self = shift;
    croak "store not implemented";
}

sub as_string {
    my $self = shift;
    return $self->package . '-'
	   . ($self->version ? $self->version : '*') . '-'
	   . ($self->arch ? $self->arch : '*') . '-'
	   . ($self->build || '1');
}

sub reconstruct {
    my $self = shift;
    my $s = $self->package . '-';
    if ($self->version) {
	$s .= $self->vpref if $self->vpref;
	$s .= $self->version;
    } else {
	$s .= '*';
    }
    $s .= '-';
    $s .= ($self->arch ? $self->arch : '*') . '-';
    $s .= ($self->build || '1');
    if ($self->tag) {
	$s .= '_'. $self->tag;
    }
    $s .= $self->suffix;
    return $s;
}

sub cmp {
    my ($self, $other) = @_;

    my $v;
    if ($v = ($self->package || '') cmp ($other->package || '')) {
	return $v;
    }
    if ($self->version <=> $other->version) {
	return $v;
    }
    if ($self->arch && $other->arch
	&& ($v = $self->arch cmp $other->arch)) {
	return $v;
    }
    return ($self->build || 1) <=> ($other->build || 1);
}   

use overload
    '""' => sub { shift->as_string },
    'cmp' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other);
	return $swap ? -$res : $res;
    },
    '<=>' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other);
	return $swap ? -$res : $res;
    },
    '==' => sub {
	my ($self, $other) = @_;
	my $res = $self->cmp($other) == 0;
    },
    '!=' => sub {
	my ($self, $other) = @_;
	my $res = $self->cmp($other) != 0;
    },
    '<' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other) < 0;
	return $swap ? !$res : $res;
    },
    '<=' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other) <= 0;
	return $swap ? !$res : $res;
    },
    '>' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other) > 0;
	return $swap ? !$res : $res;
    },
    '>=' => sub {
	my ($self, $other, $swap) = @_;
	my $res = $self->cmp($other) >= 0;
	return $swap ? !$res : $res;
    };

1;
