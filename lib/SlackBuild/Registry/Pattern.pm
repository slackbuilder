# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Registry::Pattern;
use strict;
use warnings;
use Carp;
use parent 'SlackBuild::Pattern';

my %order = (
    package => 1,
    version => 2,
    arch => 3,
    build => 4,
    filename => 5,
    date => 6
);

sub predcmp {
    my ($self, $a, $b) = @_;
    ($order{ (keys(%{$a}))[0]} || 100) <=>
	($order{(keys(%{$b}))[0]} || 100);
}

sub package {
    my $self = shift;
    my $h = $self->get_predicate('package') or return;
    return unless (keys(%$h))[0] eq '-eq';
    return (values(%$h))[0];
}

1;

