# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Rc;
use strict;
use warnings;
use Carp;
use SlackBuild::Registry::Record;
use SlackBuild::Registry::Pattern;
use Log::Log4perl;

sub new {
    my ($class, $builder) = @_;
    return bless { _builder => $builder }, $class;
}

sub logger { shift->{_logger} //= Log::Log4perl::get_logger(__PACKAGE__) }

sub builder { shift->{_builder} }

sub resolve {
    my ($self, $reg) = @_;
    my @packages;
    my @unresolved;
    foreach my $pkg (@{$self->builder->prereq_full}) {
	my %q;
	if (ref($pkg) eq 'HASH') {
	    %q = %$pkg;
	} else {
	    $q{package} = $pkg;
	}
	$q{arch} = { -in => [ $self->builder->arch, 'noarch' ] }
	             unless exists $q{arch};
	my $pred = new SlackBuild::Registry::Pattern(%q);
	if (my $rec = $reg->lookup($pred)) {
	    push @packages, $rec->filename;
	} else {
	    push @unresolved, $pred->as_string;
	}
    }
    return (\@packages, \@unresolved);
}

sub code {
    my ($self, $reg) = @_;
    my ($pkglist, $unresolved) = $self->resolve($reg);
    if (@$unresolved) {
	$self->logger->error("$_: package not resolved") for (@$unresolved);
	return;
    }
    my @code = ('#! /bin/sh',
		'set -e',
		'(. /etc/os-release; echo SLACKBUILDER: DISTRO $NAME; echo SLACKBUILDER: VERSION $VERSION)',
		map { "/sbin/upgradepkg --install-new /var/pkg/$_" } @$pkglist);

    my $env = $self->builder->environ;
    while (my ($k,$v) = each %$env) {
	next unless defined($v);
	$v =~ s/[`"]/\\"/g;
	push @code, "$k=\"$v\"", "export $k";
    }

    if (my $rc = $self->builder->rc) {
	if (ref($rc) eq 'ARRAY') {
	    push @code, @$rc;
	} else {
	    push @code, split /\n+/, $rc;
	}
    }
    
    push @code, 'exec /bin/sh $@';
    return \@code;
}

sub write {
    my ($self, $reg, $file) = @_;
    my $code = $self->code($reg) or return;
    open(my $fd, '>', $file)
	or croak "can't open \"$file\" for writing: $!";
    print $fd join("\n", @$code)."\n";
    unless (chmod(0755, $fd)) {
	croak "can't chmod \"$file\": $!";
    }
    close $fd;
    return 1;
}

1;
