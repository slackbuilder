# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Config;
use strict;
use warnings;
use parent 'Config::Parser::Ini';
use File::Spec;

sub mangle {
    my $self = shift;
    my $rootdir = $self->get(qw(dir root));
    foreach my $kw ($self->names_of('dir')) {
	next if $kw eq 'root';
	my $subdir = $self->get('dir', $kw);
	unless (File::Spec->file_name_is_absolute($subdir)) {
	    $self->set('dir', $kw, File::Spec->catfile($rootdir, $subdir));
	}
    }
    
    if ($self->is_set('lookup')) {
	if (my $v = $self->get(qw(lookup order))) {
	    my @mods = split /\s+/, $v;
	    for (my $i = 0; $i <= $#mods; $i++) {
		$self->set('lookup', $mods[$i], 'priority', $i);
	    }
	}
	    
	foreach my $name ($self->names_of('lookup')) {
	    if ($self->is_section('lookup', $name)) {
#		print "MOD $name\n";
		my $node = $self->getnode('lookup', $name);
		my $mod = "SlackBuild::Request::Loader::$name";
		(my $file = $mod) =~ s{::}{/}g;
		unless (eval { require "$file.pm" }) {
		    $self->error("can't load module $name: $@",
				 locus => $node->locus);
		    next;
		}
		eval {		    
		    $mod->Configure(%{$node->as_hash});
		};
		if ($@) {
		    $self->error("can't initialize module $name: $@",
				 locus => $node->locus);
		}
	    }
	}
    }
}

sub check_dir {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    unless (-d $val) {
	$self->error("$val: directory does not exist", locus => $locus);
	return 0;
    }
    return 1;
}

sub check_slaptget_versions {
    my ($self, $valref, $prev_value, $locus) = @_;
    my $val = $$valref;
    my %valid = (
	never => 1,
	always => 1,
	'when-available' => 1
    );
    unless ($valid{$$valref}) {
	$self->error("$val: unrecognozed keyword", locus => $locus);
	return 0;
    }
    return 1;
}	

1;
__DATA__
[core]
    image = STRING :mandatory
    verbose = NUMBER :mandatory 0
    logging = STRING :mandatory /etc/slackbuilder/logging.conf
    tag = STRING
[dir]
    root = STRING :check=check_dir :mandatory /srv/slackbuild
    tmp = STRING :mandatory tmp
    log = STRING :mandatory log
    pkg = STRING :mandatory pkg
    spool = STRING :mandatory spool
[docker]
    unconfined = BOOL
[lookup]
    order = STRING
[lookup file]
    path = STRING
    suffixes = STRING
[lookup dir]
    path = STRING
[slapt-get]
    enable = BOOL
    versions = STRING :check=check_slaptget_versions :mandatory when-available
    force = BOOL

