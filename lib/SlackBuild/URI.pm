# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::URI;
use strict;
use warnings;
use URI;
use Carp;
use LWP::UserAgent;
use File::Basename;
use File::Spec;
use Log::Log4perl;
use SlackBuild::Download;

# use LWP::Protocol::https;
# use LWP::Protocol::file;
# use LWP::Protocol::ftp;

our $AUTOLOAD;

sub AUTOLOAD {
    my $self = shift;

    # The Perl interpreter places the name of the
    # message in a variable called $AUTOLOAD.

    # DESTROY messages should never be propagated.
    return if $AUTOLOAD =~ /::DESTROY$/;

    # Remove the package name.
    $AUTOLOAD =~ s/^.*:://;
    
    # Pass the message to the delegate.
    return $self->{_uri}->$AUTOLOAD(@_);
}

sub DESTROY { }

sub _valid_scheme {
    my $scheme = shift->scheme;
    return eval { require "LWP/Protocol/$scheme.pm" }; 
}

sub new {
    my $class = shift;
    my $uri = new URI(@_);
    $uri->scheme('file') unless $uri->scheme;
    my $self = bless { _uri => $uri }, $class;
    croak "$self: unsupported scheme"
    	unless $self->_valid_scheme;
    return $self;
}

sub logger {
    my $self = shift;
    return $self->{_logger} //= Log::Log4perl::get_logger(__PACKAGE__);
}

sub clone {
    my $self = shift;
    return new SlackBuild::URI($self);
}

sub new_abs {
    my ($self, $arg) = @_;
    my $s = $self->clone;
    $s->path(File::Spec->catfile($self->path, $arg));
    return $s;
}

use overload '""' => sub { shift->as_string };

sub download {
    my $self = shift;
    my $dst = shift || basename($self->path);
    my $scheme = $self->scheme;
    require "LWP/Protocol/$scheme.pm";
    $self->logger->info("downloading $self");
    my $ua = LWP::UserAgent->new();
    $ua->agent("Slackbuilder/$SlackBuilder::VERSION");
    my $response = $ua->get($self->as_string, ':content_file' => $dst);
    my $result = new SlackBuild::Download($self,
 	                                  success => $response->is_success);
    if ($response->is_success) {
	$result->content_type(($response->content_type)[0]);
    } else {
	$self->logger->error("$self: " . $response->status_line);
    }
    return $result;
}

sub get {
    my $self = shift;
    my $scheme = $self->scheme;
    require "LWP/Protocol/$scheme.pm";
    my $ua = LWP::UserAgent->new();
    $ua->agent("Slackbuilder/$SlackBuilder::VERSION");
    my $response = $ua->get($self->as_string);
    if ($response->is_success) {
	return $response->decoded_content;
    } else {
	$self->logger->error("$self: " . $response->status_line);
    }
}   

1;
