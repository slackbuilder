package SlackBuild::Runcap;
use strict;
use warnings;
use POSIX::Run::Capture qw(:std);
use Log::Log4perl;
use POSIX qw(:sys_wait_h strerror);
use Carp;

=head1 NAME

SlackBuild::Runcap - interface class to POSIX::Run::Capture

=head1 DESCRIPTION

This is a wrapper class around L<POSIX::Run::Capture>, which provides
diagnostics via L<Log::Log4perl>.

To run a command, create a new B<SlackBuild::Runcap> object with
the same arguments as you would pass to B<POSIX::Run::Capture-E<gt>new>,
e.g.:

    my $rc = new SlackBuild::Runcap(argv => [ 'tar', @argv ]);

This will run the command, wait for its termination and return an
object which provides, among others, the same methods as
B<POSIX::Run::Capture> for accessing exit status, standard error and
output, etc.  If the execution fails, appropriate diagnostics will be
issued using L<Log::Log4perl>.

The execution status can be checked using the B<success> method, which
return true if the program was run successfully and exited with status
0, and false otherwise.

=cut

sub new {
    my $class = shift;
    my $self = bless { _rc => POSIX::Run::Capture->new(@_) }, $class;
    $self->run;
    $self;
}

sub run {
    my ($self) = @_;
    $self->{_success} = 1;
    if ($self->{_rc}->run) {
	if (WIFEXITED($self->status)) {
	    return if $self->status == 0;
	    while (my $s = $self->next_line(SD_STDERR)) {
		chomp($s);
		$self->error($s);
	    }
	    $self->error("program "
			 . $self->program
			 . " terminated with code "
			 . WEXITSTATUS($self->status));
	} elsif (WIFSIGNALED($self->status)) {
	    $self->error("program "
			 . $self->program
			 . " terminated on signal "
			 . WTERMSIG($self->status));
	} else {
	    $self->error("program "
			 . $self->program
			 . " terminated with unrecogized code "
			 . $self->status);
	}
    } else {
	$self->error("can't run "
		     . $self->program
		     . ': '
		     . strerror($self->errno));
    }
    $self->{_success} = 0;
}

sub logger { shift->{_logger} //= Log::Log4perl::get_logger(__PACKAGE__) }
sub error { shift->logger->error(@_) }
sub success { shift->{_success} }

our $AUTOLOAD;
sub AUTOLOAD {
    my $self = shift;
    $AUTOLOAD =~ s/^.*::?(.+)//;
    return $self->{_rc}->${\$1}(@_);
}

sub DESTROY {}

1;
