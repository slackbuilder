# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Base;
use strict;
use warnings;
use Carp;
use parent 'Exporter';
no strict 'refs';

sub import {
    my $pkg = shift;            # package
    my ($package, $filename, $line) = caller;
    foreach my $dfn (@_) {
	if ($dfn =~ m/^(?<attr>[a-zA-Z_][a-zA-Z_0-9]*)
                       (?::(?<flag>.*))?$/x) {
	    my $attribute = $+{attr};
	    my @kw = split ',', $+{flag} || 'rw';
	    my %flags;
	    @flags{@kw} = (1) x @kw;
	    if ($flags{ro}) {
		*{ $package . '::' . $attribute } =
		    $flags{array}
		    ? sub {
			my $self = shift;
			croak "too many arguments for ${package}::$attribute"
			    if @_;
		        return @{$self->{$attribute}};
		    }
		    : sub {
			my $self = shift;
			croak "too many arguments for ${package}::$attribute"
			    if @_;
			return $self->{$attribute};
		    }
	    } else {
		*{ $package . '::' . $attribute } =
		    $flags{array}
		    ? sub {
			my $self = shift;
			if (@_) {
			    @{$self->{$attribute}} = @_;
			}
		        return @{$self->{$attribute}};
		    }
		    : sub {
			my $self = shift;
			if (@_) {
			    croak "too many arguments for ${package}::$attribute"
				if @_ > 1;
			    $self->{$attribute} = shift;
			}
			return $self->{$attribute};
		    }
	    }
	} else {
	    croak "$filename:$line: bad attribute spec: $dfn"
	}
    }
}

1;

