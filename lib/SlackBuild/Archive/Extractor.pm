# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Archive::Extractor;
use SlackBuild::Archive::Extractor::HTTP;
use SlackBuild::Archive::Extractor::Tar;
use Carp;

sub new {
    croak "bad number of arguments" unless @_ == 4;
    my ($class, $dn, $file, $destdir) = @_;
    if ($dn->isa('SlackBuild::Download')) {
	if ($dn->is_html) {
	    return new SlackBuild::Archive::Extractor::HTTP($dn->archive,
							    $file, $destdir);
	} else {
	    return new SlackBuild::Archive::Extractor::Tar($dn->archive,
							    $file, $destdir);
	}
    }
    return bless { _archive => $dn,
		   _tempfile => $file,
		   _destdir => $destdir }, $class;
}

sub logger {
    my $self = shift;
    $self->{_logger} //= Log::Log4perl::get_logger(ref($self))
}

sub archive  { shift->{_archive} }
sub destdir  { shift->{_destdir} }
sub tempfile { shift->{_tempfile} }
sub rewind { seek shift->tempfile, 0, 0 }

sub extract {
    my $self = shift;
    $self->logger->error('unrecognized download format');
    return new SlackBuild::Download($self)
}

1;
