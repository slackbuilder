# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Archive::Extractor::HTTP;
use strict;
use warnings;
use parent 'SlackBuild::Archive::Extractor';
use SlackBuild::Download;
use HTML::Parser;
use File::Basename;
use File::Temp;
use File::Spec;
use File::Path qw(make_path);
use Carp;

sub extract {
    my $self = shift;

    my $dst = $self->destdir;
    my $result = new SlackBuild::Download($self->archive);
    unless ($self->_html_list($self->tempfile)) {
	$self->logger->error($self->archive . ": bad index file");
	return $result;
    }
    
    return $result unless $self->archive->verify;

    for (my $i = 0; my $file = $self->archive->dir($i); $i++) {
	my $subdir = dirname($file);
	my $dir = File::Spec->catfile($dst, $subdir);
	unless (-d $dir) {
	    make_path($dir, { error => \my $err });
	    if (@$err) {
		for my $diag (@$err) {
		    my ($file, $message) = %$diag;
		    if ($file eq '') {
			$self->logger->error("general error: $message");
		    } else {
			$self->logger->error("can't create $file: $message");
		    }
		}
		return $result;
	    }
	}
	my $uri = $self->archive->new_abs($file);
	my ($dh,$tmp) = File::Temp::tempfile(DIR => $dir, UNLINK => 1);
	chmod 0644, $dh;
	my $subres = $uri->download($tmp);
	unless ($subres) {
	    return $result;
        }
	my $destfile = File::Spec->catfile($dst, $file);
	if ($subres->is_html && $self->_html_list($tmp, $file)) {
	    ;
	} else {
	    rename $tmp, $destfile
		or $self->logger->error("can't rename $tmp to $destfile: $!");
        }
	close $dh;
    }
    $result->success(1);
    return $result;
}
    
sub _html_list {
    my ($self, $filename, $dir) = @_;

    my $p = HTML::Parser->new(
	api_version => 3,
	start_h => [
	    sub {
		my ($attr) = @_;
		return unless $attr->{href};
		my $f = $attr->{href};
		return if $f =~ m{(^([?#/])|(://))};
		$f = File::Spec->catfile($dir, $f) if $dir;
		$self->archive->add_file($f);
	    },
	    'attr'
	]);
    $p->report_tags(qw(a));
    return $p->parse_file($filename);
}   

1;
