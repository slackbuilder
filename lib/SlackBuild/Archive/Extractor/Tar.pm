# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Archive::Extractor::Tar;
use strict;
use warnings;
use parent 'SlackBuild::Archive::Extractor';
use SlackBuild::Download;
use SlackBuild::Archive::Tar;

sub extract {
    my $self = shift;
    my $result = new SlackBuild::Download($self->archive);
    my $tar = new SlackBuild::Archive::Tar($self->tempfile);
    $tar->workdir($self->destdir);
    $result->success($tar->strip($self->archive->package_name)
		     && $self->archive->add_file($tar->list)
		     && $self->archive->verify);
    return $result;
}

1;
