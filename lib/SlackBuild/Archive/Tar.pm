package SlackBuild::Archive::Tar;
use strict;
use warnings;
use SlackBuild::Runcap;
use POSIX::Run::Capture qw(:std);
use File::Basename;
use File::Temp qw(tempfile tempdir);
use File::Spec;
use Log::Log4perl;
use Carp;

=head1 NAME

SlackBuild::Archive::Tar - domain-specific API for accessing tar archives

=head1 DESCRIPTION

This module provides API for accessing (reading or appending to)
slackware package tar archives.  It uses GNU tar as the underlying agent.

=head1 CONSTRUCTOR

    $tar = new SlackBuild::Archive::Tar($file);

Creates an object for read-only access to tar archive B<$file>.
The returned object can be used to get the listing of the archive,
and extract files from it (see the B<extract> and B<strip> methods),
but cannot be used to alter the archive content.    
    
    $tar = new SlackBuild::Archive::Tar($file, append => 1);

Creates an object for reading and appending to tar archive B<$file>.
The returned object cannot be used for extraction.    

=cut

sub new {
    my ($class, $filename, %opt) = @_;
    bless { _filename => $filename, _append => $opt{append} }, $class;
}

=head1 METHODS

=head2 filename

Returns the archive filename.

=cut

sub filename { shift->{_filename} }

=head2 is_mode_append

Returns true if the archive was created for appending.

=cut

sub is_mode_append { shift->{_append} }

my %compressor = (
    gz => 'gzip',
    tbz => 'bzip2',
    bz2 => 'bzip2',
    tlz => 'lzma',
    lzma => 'lzma',
    txz => 'xz',
    xz => 'xz'
);

sub uncompress {
    my ($self) = @_;
    unless ($self->{_plaintar}) {
	my ($name,undef,$suffix) =
	    fileparse($self->filename, keys(%compressor));
	my $plaintar = File::Spec->catfile($self->workdir, $name);
	my $rc = SlackBuild::Runcap->new(
	    argv => [ $compressor{$suffix}, '-d', '-c', $self->filename ],
	    stdout => $plaintar);
	if ($rc->success) {
	    $self->{_plaintar} = $plaintar;
	}
    }
    return $self->{_plaintar};
}

sub tarcmd {
    my ($self, @argv) = @_;
    local $ENV{TAR_OPTIONS};
    unshift @argv, '-f', $self->filename;
    my (undef,undef,$suffix) = fileparse($self->filename, keys(%compressor));
    if ($suffix) {
	unshift @argv, '-I', $compressor{$suffix};
    }
    my $rc = SlackBuild::Runcap->new(argv => [ 'tar', @argv ]);
    if ($rc->success) {
	[map { chomp; s{/$}{}; $_ } @{ $rc->get_lines(SD_STDOUT) }];
    }
}

=head2 list

    @a = $tar->list;

Returns a list of member names in archive.

=cut

sub list {
    my ($self) = @_;
    if (!$self->{_list}) {
	if (my $out = $self->tarcmd('-t')) {
	    $self->{_list} = $out;
	} else {
	    $self->{_list} = [];
	}
    }
    if ($self->{_sort}) {
	$self->{_list} = [sort @{$self->{_list}}];
	$self->{_sort} = 0;
    }
    return @{$self->{_list}};
}

=head2 has_file

    if ($tar->has_file($file)) {
       ...

Returns true if B<$file> is a member of the archive B<$tar>.  B<$file> must
be given exactly as it should appear in the archive, but without trailing
slash, if it is a directory,

=cut

sub has_file {
    my ($self, $file) = @_;
    grep { $_ eq $file } $self->list;
}

=head2 match_files

    @files = $tar->match_files(@patterns);

Returns a list of archive members whose names match at least on of the regexps
from B<@patterns>.

=cut

sub match_files {
    my $self = shift;
    grep { my $file = $_;
	   grep { $file =~ m{$_} } @_ } $self->list;
}

=head2 workdir

    $d = $tar->workdir;

Returns the working directory for this archive.  This is the directory where
the archive will be extracted (or is already extracted) by B<extract> or
B<strip> methods.

    $tar->workdir($newdir);

If called with an argument sets the working directory and creates it if
it does not exist.  Such invocation can happen only once, before any
of the following methods were called: B<workdir>, B<extract>, B<strip>,
B<add_file>, B<add_file_content>.
If the directory has not been set, a call to any of these functions
will create a temporary directory which will be removed when the object
is destroyed.

=cut

sub workdir {
    my $self = shift;
    if (@_) {
	if ($self->{_workdir}) {
	    croak "working directory already set ($self->{_workdir})";
	}
	$self->{_workdir} = shift;
	if (! -d $self->{_workdir}) {
	    mkdir $self->{_workdir} or
		croak "can't create directory $self->{_workdir}: $!";
	}
    }
    if (!$self->{_workdir}) {
	$self->{_workdir} = tempdir(CLEANUP => 1);
    }
    return $self->{_workdir};
}

=head2 extract

    $tar->extract;

Extracts the archive to the working directory.  On success, returns the
name of the working directory.  On error, logs the reason using
B<Log::Log4perl> and returns false.

It is safe to call this method multiple times.  Once the archive has
been extracted, any subsequent call to B<extract> is a no-op.

=cut

sub extract {
    my $self = shift;
    croak "can't extract from ".$self->filename.": archive open for appending"
	if $self->is_mode_append;
    if ($self->extracted) {
	return $self->workdir;
    }
    if ($self->tarcmd('-x', '-C', $self->workdir, @_)) {
	$self->{_extracted} = 1;
	return $self->workdir;
    }
}

=head2 strip

    $tar->strip($prefix)

If all archive members are located under given directory B<$prefix>,
extracts the archive stripping this prefix from each member name.
Otherwise it is equivalent to B<extract>.

=cut

sub strip {
    my ($self, $pfx) = @_;
    my @list;
    foreach my $member ($self->list) {
	if ($member =~ m{\Q$pfx\E(?:/(.*))?$}) {
	    next unless $1;
	    push @list, $1;
	} else {
	    @list = ();
	}
    }
    my @args;
    if (@list) {
	$self->{_list} = \@list;
	push @args, '--strip=1';
    }
    $self->extract(@args);
}

# sub strip {
#     my $self = shift;
#     my @list = grep { $_ ne '.' } $self->list;
#     return unless @list;
#     my $prefix = (File::Spec->splitdir(shift(@list)))[0];
#     while (my $member = shift @list) {
#	my @c = File::Spec->splitdir($member);
#	return unless $c[0] eq $prefix;
#     }
#     $self->extract or croak $self->filename . ": extraction failed";
#     $self->{_workdir} = File::Spec->catfile($self->{_workdir}, $prefix);
#     return $self->{_workdir};
# }

=head2 extracted

Returns true if the archive has been extracted.

=cut

sub extracted { shift->{_extracted} }

=head2 changed

Returns true if the archive has been modified by a call to B<add_file> or
B<add_file_content>.

=cut

sub changed { shift->{_changed} }

sub logger { shift->{_logger} //= Log::Log4perl::get_logger(__PACKAGE__) }

=head2 add_file

    $tar->add_file($filename, $membername);

Reads the file B<$filename> and adds its content to the archive as the
member <$membername>.  The B<$tar> object must have been created with
the B<append =E<gt> 1> option.

The appended member will be owner by root and will have the mode of
0644.    
    
On success, returns true.  On failure, returns false and logs the
reason using B<Log::Log4perl>.

=cut

sub add_file {
    my ($self, $filename, $membername) = @_;
    croak "can't append to ".$self->filename.": archive open for extraction"
	unless $self->is_mode_append;
    my $tarfile = $self->uncompress or return;
    (my $s_name = $filename) =~ s{^/}{};
    $s_name =~ s{([\\/|()\[\]{}^\$\*+?.])}{\\$1}g;
    (my $d_name = $membername) =~ s{/}{\\/}g;
    my $rc = SlackBuild::Runcap->new(
	argv => [ 'tar', '-r', '-f', $tarfile,
		  '--owner=root:0', '--group=root:0',
		  "--transform=s/$s_name/$d_name/",
		  '--mode=644',
		  $filename ]);
    if ($rc->success) {
	push @{$self->{_list}}, $membername;
	$self->{_sort} = 1;
	$self->{_changed} = 1;
    }
    return $rc->success;
}

=head2 add_file_content

    $tar->add_file($membername, $content);

Adds B<$content> to the archive under the name B<$membername>. 
    
The appended member will be owner by root and will have the mode of
0644.

On failure, returns false and logs the reason using B<Log::Log4perl>.

=cut

sub add_file_content {
    my ($self, $membername, $text) = @_;
    croak "can't append to ".$self->filename.": archive open for extraction"
	unless $self->is_mode_append;
    my ($fh, $filename) = tempfile(DIR => $self->workdir);
    print $fh $text;
    close $fh;
    return $self->add_file($filename, $membername);
}

=head2 save

    $tar->save;

Saves the archive after modifications.  Returns true on success, false on
failure.

This method is always called when destroying the B<SlackBuild::Archive::Tar>
object.

=cut

sub save {
    my $self = shift;
    return 1 unless $self->changed;
    unless (-w $self->filename) {
	unlink $self->filename;
    }

    my ($name,undef,$suffix) =
	fileparse($self->filename, keys(%compressor));
    my $rc = SlackBuild::Runcap->new(
	argv => [ $compressor{$suffix}, '-c', $self->uncompress ],
	stdout => $self->filename);
    if ($rc->success) {
	$self->{_changed} = 0;
    }
    return $rc->success;
}

sub DESTROY { shift->save }

1;
