# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Counter;
use strict;
use warnings;
use Carp;

=head1 NAME

Slackbuild::Counter - collection of increasing counters

=head1 SYNOPSIS

    $c = new SlackBuild::Counter;
    $c->more('errors');
    $c += 'errors';
    $c->more('warnings');
    if ($c) {
       print "there are messages: ".join(',',$c->categories)."\n";
       print "total: $c\n";
    }
    $c->clear;

=head1 DESCRIPTION

Holds a collection of counters. Counters are named by arbitrary strings,
called I<categories>. Counters spring into existence when they are
I<increased> and are destroyed when they are I<cleared>.
    
To increase, each counter use the B<incr>, or B<more> method:

    $c->more('errors')

The above increases it C<errors> counter by 1. The same effect can be
achieved using the following construct:

    $c += 'errors'

To get the names of existing categories (i.e. categories with non-zero
counters), use

    $c->categories()

In scalar context, it returns the number of existing categories.

To get the counter for a given category (e.g. 'errors'), use
the B<get> method. If you are sure that the category exists (you can
check it via B<exists>), then the name of the category can be used as
a method of this class:

    if ($c->exists('messages')) {
       print $c->messages;
    }

To get the total of all counters, use the B<total> method, or simply B<$c> in
scalar context.

=cut

sub new { bless {}, shift }

=head1 METHODS

=head2 incr, more

    $c->incr($category)
    $c->more($category)
    $c += $category;

Increment counter for the given I<$category>.

=cut    
    
sub incr {
    my ($self, $category) = @_;
    $self->{$category}++;
}

sub more { shift->incr(@_) }

=head2 clear

    $c->clear;
    $c->clear(@names);

Without arguments, clear all counters. With arguments, clear only counters
for named categories.

=cut    

sub clear {
    my $self = shift;
    if (@_) {
	delete @$self{@_};
    } else {
	delete @$self{(keys %$self)};
    }
}

=head2 categories

    $n = $c->categories();

Return the number of defined categories.

=cut

sub categories {
    my $self = shift;
    return keys %$self;
}

=head2 exists

    if ($c->exists($category)) {
       ...
    }

Return true if the B<$category> exists.

=cut    

sub exists {
    my ($self, $category) = @_;
    return exists $self->{$category};
}

=head2 get

    $n = $c->get($category)

Return the counter value for the specified category. Return C<undef>
if the category does not exist.

You can also use the name of the category as the class method for this
purpose. For example, to get the C<errors> counter:

    $n = $c->errors

The difference is that it will croak if there is no such counter.
    
=cut

sub get {
    my ($self, $category) = @_;
    return unless $self->exists($category);
    return $self->{$category};
}

=head2 total

    $n = $c->total;

Return the total number of all counters. The same value is returned
if B<$c> is used in scalar context.    

=cut    

sub total {
    my $self = shift;
    my $total = 0;
    map { $total += $_ } values %$self;
    return $total;
}

our $AUTOLOAD;

sub AUTOLOAD {
    my $self = shift;
    (my $category = $AUTOLOAD) =~ s/^.*:://;
    if (defined(my $n = $self->get($category))) {
	return $n;
    }
    croak "no such category";
}

sub DESTROY { }

use overload
    "0+" => sub { shift->total },
    "bool" => sub { shift->categories() },
    "+"  => sub {
	my ($self, $other) = @_;
	$self->more($other);
	return $self;
    },
    "==" => sub {
	my ($self, $other) = @_;
	$self->total == $other;
    },
    "eq" => sub {
	my ($self, $other) = @_;
	$self->total eq $other;
    };

1;

	    
    
