# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Download;
use strict;
use warnings;
use SlackBuild::Base qw(archive success content_type);
use SlackBuild::Archive::Extractor;
use Carp;

=head1 NAME

SlackBuild::Download - download result class

=head1 DESCRIPTION

Objects of this class represent the return type of download operations.
Each object has two mandatory associated attributes: archive that performed
the download (a SlackBuild::Archive object) and the success marker (true or
false). When used in scalar context, B<SlackBuild::Download> behaves as a
boolean value (true if the download succeeded and false otherwise),    

=head1 CONSTRUCTOR

    $result = new SlackBuild::Download($archive);

Creates a failed download objects. The object will yield B<false> when used
in scalar context.
    
    $result = new SlackBuild::Download($archive, success => 1);

Creates a success download object. This object will evaluate to B<true> when
used in scalar context.

Any of the object attributes can be initialized using keyword arguments. E.g.:

    $result = new SlackBuild::Download($archive, success => 1,
                                       content_type => 'text/plain');

=cut

sub new {
    my $class = shift;
    my $archive = shift or croak "too few arguments";
    my $self = bless {}, $class;
    $self->archive($archive);
    croak "odd number of arguments"
	if @_ % 2;
    local %_ = @_;
    while (my ($k,$v) = each %_) {
	if (my $method = $self->can($k)) {
	    $self->${\$method}($v);
	} else {
	    croak "unknown keyword $k"
	}
    }
    return $self;
}

=head1 ATTRIBUTES

=head2 archive

    $a = $self->archive;
    $self->archive($a);

Return or set and return the associated archive.

=head2 success    

Return or set and return the success status (boolean value).

=head2 content_type

Return or set and return the content type (string).

=head2 is_html

    if ($dn->is_html) {
        ...
    }

Return B<true> if downloaded object is an HTML page.

=cut    

sub is_html {
    my $self = shift;
    defined($self->content_type)
	&&
    $self->content_type =~ m{^(?:application/xhtml\+xml|text/(?:css|html))$};
}

sub extract {
    my $self = shift;
    SlackBuild::Archive::Extractor->new($self, @_)->extract;
}

use overload
    "bool" => sub { shift->success },
    "0+"   => sub { shift->success },
    '""'   => sub { shift->success ? "" : "ok" };

1;
