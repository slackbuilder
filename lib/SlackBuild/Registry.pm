# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Registry;
use strict;
use warnings;
use Carp;
use Scalar::Util qw(blessed);
use SlackBuild::Base qw(backend:ro);
use SlackBuild::Registry::Pattern;

=head1 NAME

SlackBuild::Registry - registry of completed builds

=head2 new

    $x = new SlackBuild::Registry(BACKEND, KW=>VAL, ...)

Creates new registry object. I<BACKEND> is the name of the backend
to use. The class B<SlackBuild::Registry::Backend::I<BACKEND>> must
exist. The arguments (I<KW =E<gt> VAL> pairs) are passed to its
constructor.    
    
=cut    

sub new {
    my $class = shift;
    my $backend = shift or croak "no backend specified";
    $backend = "SlackBuild::Registry::Backend::$backend";
    eval "use $backend";
    croak $@ if $@;
    my $self = bless {}, $class;
    $self->{backend} = $backend->new(@_);
    return $self;
}

=head2 lookup

    @a = $x->lookup($pattern)

Returns a sorted array of SlackBuild::Registry::Record objects matching the
B<SlackBuild::Registry::Pattern> object B<$pattern>.

Invoking B<lookup> with a hash as arguments:
    
    @a = $x->lookup(%hash)

is equivalent to

    @a = $x->lookup(SlackBuild::Registry::Pattern->new(%hash))

Finally, the following invocation:
    
    @a = $x->lookup($name, %hash)

is equivalent to    
    
    @a = $x->lookup(package => $name, %hash)

=cut

sub lookup {
    my $self = shift;
    my $pattern;

    croak "no predicate" unless @_;
    if (blessed($_[0])) {
	$pattern = shift;
	croak "invalid argument type"
	    unless $pattern->isa('SlackBuild::Registry::Pattern');
	croak "excess arguments"
	    if @_;
    } elsif (@_ % 2 == 0) {
	$pattern = new SlackBuild::Registry::Pattern(@_);
    } else {
	my $package = shift;
	$pattern = new SlackBuild::Registry::Pattern(package => $package, @_);
    }

    return $self->backend->lookup($pattern);
}

use overload
    '@{}' => sub { shift->backend->getlist(@_) };

1;




    
