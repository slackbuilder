package SlackBuild::Request::Loader;
use strict;
use warnings;
use Text::ParseWords;
use Carp;
use Log::Log4perl qw(:levels);

our $PRIORITY = 0;
our $MAX_PRIORITY = 0;

sub Configure {
    my $pkg = shift;
    local %_ = @_;
    if (defined(my $v = delete $_{priority})) {
#	print "pkg=$pkg $v\n";
	no strict 'refs';
	${ $pkg.'::PRIORITY' } = $v;
	$MAX_PRIORITY = $v if $v > $MAX_PRIORITY;
    }
    croak "unrecognized arguments" if keys %_;
}

sub logger {
    Log::Log4perl::get_logger(__PACKAGE__);
}

1;

