# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Request::Loader::file;
use strict;
use warnings;
use JSON;
use YAML ();
use Carp;
use Text::ParseWords;
use File::Spec;
use File::Basename;
use parent 'SlackBuild::Request::Loader';

our $PRIORITY = 10;

my @PATH;
my @SUFFIXES = qw(.yml .yaml);

sub Configure {
    my $pkg = shift;
    local %_ = @_;
    if (my $v = delete $_{path}) {
	@PATH = parse_line('\s+', 0, $v);
    }
    if (my $v = delete $_{suffixes}) {
	@SUFFIXES = parse_line('\s+', 0, $v);
    }
    $pkg->SUPER::Configure(%_);
}

sub Load {
    my ($class, $reqname) = @_;
    my @c = File::Spec->splitdir($reqname);
    my ($filename,$dir,$suffix) = fileparse($reqname, @SUFFIXES);	
    my @searchpath = (@c == 1) ? @PATH : ( $dir );
    my @suffixes = $suffix ? ( $suffix ) : @SUFFIXES;
    foreach my $file (map { my $filename = File::Spec->catfile($_, $filename);
			    map { $filename . $_ } @suffixes } @searchpath) {
	if (-f $file) {
	    local $/ = undef;
	    open(my $fd, $file) or croak "can't open file $file: $!";
	    my $string = <$fd>;
	    close $fd;
	    if ($string =~ /^\{/) {
		return decode_json($string);
	    }
	    return YAML::Load($string);
	}
    }
}   

1;

