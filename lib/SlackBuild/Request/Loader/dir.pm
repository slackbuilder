# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Request::Loader::dir;
use strict;
use warnings;
use File::Basename;
use File::Spec;
use Text::ParseWords;
use parent 'SlackBuild::Request::Loader';

our $PRIORITY = 20;
our @PATH = ( '.' );

sub Configure {
    my $pkg = shift;
    local %_ = @_;
    if (my $v = delete $_{path}) {
	@PATH = parse_line('\s+', 0, $v);
    }
    $pkg->SUPER::Configure(%_);
}

sub Load {
    my ($class, $reqname) = @_;
    my @path;
    if (($reqname =~ m{^\.\.?/} || File::Spec->file_name_is_absolute($reqname))
	&& -d $reqname) {
	push @path, dirname($reqname);
	$reqname = basename($reqname);
    } else {
	@path = @PATH;
    }
    foreach my $dir (@path) {
	if (my $file =
	    (glob File::Spec->catfile($dir, $reqname, '*.SlackBuild'))[0]) {
	    my ($package,$path) = fileparse($file, '.SlackBuild');
	    return { package => $package, slackbuild_uri => $path };
	}
    }
}

1;

