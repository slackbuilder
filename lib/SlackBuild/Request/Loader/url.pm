# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Request::Loader::url;
use strict;
use warnings;
use URI;
use parent 'SlackBuild::Request::Loader';

our $PRIORITY = 0;

sub Load {
    my ($class, $reqname) = @_;
    if ($reqname =~ m{^\w+://}) {
	my $uri = new SlackBuild::URI($reqname);
	if ($uri->scheme =~ m{^(?:http|ftp)s?}) {
	    if ($uri->path =~ m{.*/(.+?)\.tar(?:\.(?:[xgl]z|bz2))?}x) {
		return { package => $1, slackbuild_uri => $reqname };
	    }
	    if (my $string = $uri->get) {
		my @pkg;
		my $p = HTML::Parser->new(
		    api_version => 3,
		    start_h => [
			sub {
			    my ($attr) = @_;
			    if ($attr->{href}
				&& $attr->{href} =~ /(.+)\.SlackBuild$/) {
				push @pkg, $1;
			    }
			},
			'attr'
		    ]);
		$p->report_tags(qw(a));
		$p->parse($string);
		if (@pkg) {
		    return { package => $pkg[0], slackbuild_uri => $reqname }
		}
	    }
	}
	if ($uri->scheme eq 'sbo' && $uri->package) {
	    return { package => $uri->package, slackbuild_uri => $reqname }
	}
    }
}

1;
