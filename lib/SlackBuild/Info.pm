# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Info;
use strict;
use warnings;
use Carp;
use Text::ParseWords;
use JSON;

=head1 NAME

SlackBuild::Info - a slackbuilds.org info object

=head1 DESCRIPTION

A representation of the B<slackbuilds.org> "info" file. See
the L<https://slackbuilds.org/guidelines/>, section B<$PRGNAM.info file>.

=cut    
    
my %attributes = (
    PRGNAM   => 1,
    VERSION  => 1,
    HOMEPAGE => 1,
    DOWNLOAD => 'ARRAY',
    MD5SUM   => 'ARRAY',
    DOWNLOAD_x86_64 => 'ARRAY',
    MD5SUM_x86_64   => 'ARRAY',
    REQUIRES => 'ARRAY',
    MAINTAINER => 1,
    EMAIL => 1
);

=head1 CONSTRUCTOR

    new SlackBuild::Info(FH)

Reads the attributes from the file handle B<FH>.

    new SlackBuild::Info(\$string)

Reads the attributes from the string B<$string>, which must contain the text
of an info file.    

    new SlackBuild::Info($filename)

Reads the attributes from the file named B<$filename>.

    new SlackBuild::Info

Returns an empty info object.

=cut    

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    if (my $arg = shift) {
	my $fd;
	if (ref($arg) eq 'GLOB') {
	    open($fd, '<&', $arg)
		or croak "can't dup filehandle: $!";
	} else {
	    open($fd, '<', $arg)
		or croak "can't open $arg: $!";
	}
	while (<$fd>) {
	    chomp;
	    s/\s+$//;
	    if (/\\$/) {
		chop;
		$_ .= <$fd>;
		redo;
	    }
	    s/^\s+//;
	    next if /^(#.*)?$/;
	    croak "bad input line: $_"
		unless /^(?<kw>[A-Za-z_][A-Za-z_0-9]*)="(?<val>.*)"$/;
	    if (exists($attributes{$+{kw}})) {
		$self->${\$+{kw}}($+{val});
	    } else {
		carp "ignoring unrecognized line: $_";
	    }
	}
	close $fd;
    }
    return $self;
}

=head2 for_arch

    $clone = $info->for_arch($arch)

Returns a clone of B<$info> with B<DOWNOAD> and B<MD5SUM> attributes adjusted
for architecture B<$arch>.

=cut    

sub for_arch {
    my ($self, $arch) = @_;
    my $clone = new SlackBuild::Info;
    my @attrs = keys %attributes;
    
    if (my $d = $self->{'DOWNLOAD_'.$arch}) {
	if ($d ne 'UNSUPPORTED') {
	    $clone->{DOWNLOAD} = $d;
	    $clone->{MD5SUM} = $self->{'MD5SUM_'.$arch};
	    @attrs = grep { !/^(DOWNLOAD|MD5SUM)/ } @attrs;
	}
    }

    foreach my $attr (@attrs) {
	if ($self->{attr} && $attributes{$attr} eq 'ARRAY') {
	    $clone->{$attr} = [ @{$self->{$attr}} ];
	} else {
	    $clone->{$attr} = $self->{$attr};
	}
    }
    return $clone;
}

=head1 STRING REPRESENTATION

When used is string context, objects of this class are represented as
JSON objects with attribute names sorted in lexical order. The same
representation is returned by the B<str> method.    

=cut

sub as_string {
    my $self = shift;
    my %h;
    my @k = keys %attributes;
    @h{@k} = map { $self->${\$_} } @k;
    JSON->new->canonical(1)->encode(\%h);
}

use overload '""' => sub { shift->as_string };

{
    no strict 'refs';
    use feature 'state';
    while (my ($attr,$type) = each %attributes) {
	*{ __PACKAGE__ . '::' . $attr } = sub {
	    my $self = shift;
	    croak "too many arguments" if @_ > 1;
	    if (my $v = shift) {
		if ($type eq 'ARRAY') {
		    $self->{$attr} = [parse_line('\s+', 0, $v)];
		} else {
		    $self->{$attr} = $v;
		}
	    }
	    return $self->{$attr};
	}
    }
}

1;
