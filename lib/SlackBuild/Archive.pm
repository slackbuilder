# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuild::Archive;
use strict;
use warnings;
use parent 'SlackBuild::URI';
use SlackBuild::Archive::Extractor;
use SlackBuild::Info;
use File::Temp qw/ tempfile tempdir /;
use File::Basename;
use File::Spec;
use Log::Log4perl;
use Carp;

our $AUTOLOAD;
*AUTOLOAD = \&SlackBuild::URI::AUTOLOAD;
sub DESTROY { }

sub new {
    my ($class, $pname) = (shift, shift);
    my $self = $class->SUPER::new(@_);
    $self->{_package_name} = $pname;
    return $self;
}

sub package_name { shift->{_package_name} }
sub dir {
    my ($self, $n) = @_;
    if (defined($n)) {
	return undef unless $n <= @{$self->{_dir}};
	return $self->{_dir}[$n];
    }
    return @{$self->{_dir}}
}

sub add_file {
    my $self = shift;
    push @{$self->{_dir}}, map {
	s{/$}{};
	File::Spec->no_upwards(basename($_)) ? $_ : ()
    } @_;
}

sub has_file {
    my ($self, $file) = @_;
    return grep { $_ eq $file } ($self->dir);
}

sub verify {
    my $self = shift;
    my $file = $self->package_name . '.SlackBuild';
    return $self->error("no $file in archive")
	unless $self->has_file($file);
    # return $self->error("no slack-desc in archive")
    # 	unless $self->has_file('slack-desc');
    return 1;
}

sub download {
    croak "bad number of arguments" unless @_ == 2;
    my ($self, $dst) = @_;
    my (undef, $tmp) = tempfile(DIR => dirname($dst), UNLINK => 1);
    my $result = $self->SUPER::download($tmp);
    if ($result) {
	$result = $result->extract($tmp, $dst);
	if ($result) {
	    $self->_read_info($dst);
	}
    }
    unlink $tmp;
    return $result;
}

sub content_type {
    my ($self, $newtype) = @_;
    if (defined($newtype)) {
	$self->{_content_type} = $newtype;
    }
    return $self->{_content_type};
}

sub _read_info {
    my ($self, $dst) = @_;
    my $info = $self->package_name . '.info';
    return unless $self->has_file($info);
    $self->{_info} = new SlackBuild::Info(File::Spec->catfile($dst, $info));
}

sub info {
    my ($self) = @_;
    return $self->{_info} ||= new SlackBuild::Info;
}

sub download_status {
    my $self = shift;
    return $self->SUPER::download_status unless exists $self->{_last_status};
    return $self->{_last_status};
}

sub logger { shift->{_logger} //= Log::Log4perl::get_logger(__PACKAGE__) }

sub error {
    my $self = shift;
    $self->{_last_status} = shift;
    $self->logger->error($self->{_last_status});
    return 0;
}

1;
