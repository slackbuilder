# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package LWP::Protocol::sbo;
use base qw(LWP::Protocol);
use strict;
use warnings;
use File::Temp;

our $VERSION = '1.00';

use HTTP::Request;
use HTTP::Response;
use HTTP::Status;

sub request {
    my ($self, $request, $proxy, $arg, $size) = @_;

    if (defined $proxy) {
	return HTTP::Response->new(HTTP::Status::RC_BAD_REQUEST,
				   'You can not proxy through the SBo archive');
    }

    unless ($request->method eq 'GET') {
	return HTTP::Response->new(HTTP::Status::HTTP_METHOD_NOT_ALLOWED,
				   "Only GET is allowed for sbo: URLs");
    }

    my $url = $request->uri;
    my $scheme = $url;
    if ($url->scheme ne 'sbo') {
	return HTTP::Response->new(HTTP::Status::RC_INTERNAL_SERVER_ERROR,
				   __PACKAGE__."::request called for '$scheme'");
    }

    my $fh = new File::Temp;
    
    eval {
	my $sbo = Net::SBo->new;
	$sbo->get($url->path, $fh->filename, treeish => $url->branch);
    };
    if ($@) {
	return HTTP::Response->new(HTTP::Status::RC_INTERNAL_SERVER_ERROR, $@);
    }

    my $response = HTTP::Response->new(HTTP::Status::RC_OK);
    $size = -s $fh;
    binmode($fh);
    $response =  $self->collect($arg, $response,
				sub {
				    my $content = "";
				    my $bytes = sysread($fh, $content, $size);
				    return \$content if $bytes > 0;
				    return \ "";
				});
    $response->header('Content-Type', 'application/x-tar');
    $response->header('Content-Length', $size);

    $response;
}

1;

    
