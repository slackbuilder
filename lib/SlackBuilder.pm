# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package SlackBuilder;
use strict;
use warnings;
use Carp;
use SlackBuild::Config;
use SlackBuild::URI;
use SlackBuild::Archive;
use SlackBuild::Registry;
use SlackBuild::Registry::Record;
use SlackBuild::Request;
use SlackBuild::Rc;
use SlackBuild::Counter;
use SlackBuild::Archive::Tar;
use SlackBuild::Runcap;
use File::Spec;
use File::Basename;
use File::Temp qw(tempfile tempdir);
use File::Copy;
use POSIX::Run::Capture qw(:all);
use POSIX qw(:sys_wait_h strerror uname);
use Log::Log4perl qw(:levels);
use Try::Tiny;
use Carp;
use Digest::MD5;

use constant {
    E_OK => 0,
    E_SYNTAX => 1,
    E_EXEC => 2,
    E_BADBUILD => 3,
    E_FAIL => 4
};

our $VERSION = '1.0';
our @EXPORT = qw(E_OK
                 E_SYNTAX
                 E_EXEC
                 E_BADBUILD
                 E_FAIL);

sub new {
    my ($class, %args) = @_;
    my $self = bless {}, $class;

    $self->{conf} = new SlackBuild::Config(
	filename => $ENV{SLACKBUILDER_CONFIG} || '/etc/slackbuilder.conf');

    Log::Log4perl::Layout::PatternLayout::add_global_cspec('N',
	 sub {
	     my ($layout, $message, $category, $priority, $caller_level) = @_;
	     my $pfx = "$0: $priority";
	     if ($priority eq 'DEBUG') {
		 $pfx .= ": $category";
		 if ($self->conf->core->verbose > 1) {
		     my $caller_offset = Log::Log4perl::caller_depth_offset($caller_level);
		     my ($package,$filename,$line) = caller($caller_offset);
		     $pfx .= ": $filename:$line";
		 }
	     }
	     return $pfx;
	 });
    $self->logger->level($INFO);
    my $layout = new Log::Log4perl::Layout::PatternLayout("%N: %m%n");
    my $appender = new Log::Log4perl::Appender('Log::Dispatch::Screen',
					       stderr => 1);
    $appender->layout($layout);
    $self->logger->add_appender($appender);
    my $v;
    if (defined($v = $self->conf->core->logging) && -f $v) {
	Log::Log4perl::init($v);
    }
    
    if ($v = delete $args{verbose}) {
	$self->conf->set(qw(core verbose), $v);
    }
    if ($self->conf->core->verbose > 0) {
	$self->logger->level($DEBUG);
    }

    if ($v = delete $args{image}) {
	$self->conf->set(qw(core image), $v);
    }

#    croak "bad number of arguments" if keys(%args);

    $self->clear;

    return $self;
}

sub conf { shift->{conf} }

sub rootdir  { shift->conf->dir->root->value };
sub spooldir { shift->conf->dir->spool->value };
sub tmpdir   { shift->conf->dir->tmp->value };
sub logdir   { shift->conf->dir->log->value };
sub pkgdir   { shift->conf->dir->pkg->value };
sub ospkgdir {
    my $self = shift;
    File::Spec->catfile($self->pkgdir, $self->osversion, @_);
}
sub image    { shift->conf->core->image->value };
sub arch     { shift->{_arch} };

sub logger { shift->{_logger} //= Log::Log4perl::get_logger('SlackBuild') }

sub error {
    my ($self, $diag) = @_;
    $self->logger->error($diag);
}

sub errno {
    my $self = shift;
    croak "bad number of arguments" if @_ > 1;
    if (my $v = shift) {
	$self->{_errno} = $v;
    }
    return $self->{_errno};
}

sub errors { shift->{_error_counter} //= new SlackBuild::Counter }

sub is_success {
    shift->errno == E_OK;
}

sub clear {
    my $self = shift;
    $self->{_errno} = E_OK;
    $self->errors->clear();
    delete $self->{_request};
    delete $self->{_result};
}

sub request {
    my $self = shift;
    croak "too many arguments" if @_ > 1;
    if (my $v = shift) {
	$self->{_request} = $v;
    }
    return $self->{_request};
}

sub package_name { shift->request->package }
sub package_version { shift->request->version }
sub package_build { shift->request->build }
sub rc { shift->request->rc }
sub slackbuild_uri { shift->request->slackbuild_uri }
sub source_uri { shift->request->source_uri }
sub prereq { shift->request->prereq || []}
sub registry { shift->{_registry} }

sub prereq_full {
    my $self = shift;

    my %h;
    # Build initial prerequisite hash.
    foreach my $r (@{$self->request->prereq || []}) {
	if (ref($r) eq 'HASH') {
	    $h{$r->{package}} = $r;
	} else {
	    $h{$r} = $r;
	}
    }

    # Apply build prerequisites, if any.
    # FIXME: Take into account $br->{arch} as well.
    # FIXME: Shouldn't the loop below be part of 'merge' policy in Request.pm?
    foreach my $br (@{$self->request->build_prereq || []}) {
	if (ref($br) eq 'HASH') {
	    if (exists($h{$br->{package}})) {
		if ($br->{version}) {
		    my $r = $h{$br->{package}};
		    if (ref($r) eq 'HASH') {
			if (SlackBuild::Registry::Version->new($r->{version}) < SlackBuild::Registry::Version->new($br->{version})) {
			    $h{$br->{package}}{version} = $br->{version}
			}
		    } else {
		        $h{$br->{package}}{version} = { package => $br->{package},
							version => $br->{version} }
		    }
		}
	    } else {
		$h{$br->{package}} = $br;
	    }
	} else {
	    $h{$br} = $br;
	}
    }
    return [ map { $h{$_} } sort keys %h ]
}

sub environ {
    my $self = shift;
    my $env = {%{$self->request->environ // {}},
	       VERSION => $self->package_version};
    if (my $b = $self->package_build) {
	$env->{BUILD} = $b;
    }
    return $env;
}

sub file {
    my ($self, $name) = @_;
    my $src = File::Spec->catfile($self->tmpdir, $name);

    if (my $tag = $self->conf->core->tag->value) {
	my ($base,$path) = fileparse($name);
	if (my $r = SlackBuild::Registry::Record->split($base)) {
	    $r->tag($tag);
	    $name = $r->reconstruct;
	} else {
	    $self->error("can't parse package name $base");
	    $self->errno(E_FAIL);
	    return;
	}
    }
    
    my $dst = $self->ospkgdir($name);

    my $dstdir = dirname($dst);
    if (! -d $dstdir) {
	unless (mkdir($dstdir)) {
	    $self->error("mkdir($dstdir): $!");
	    $self->errno(E_FAIL);
	    return;
	}
    }
    
    if (-e $dst) {
	unlink $dst;
    }
    if (move($src, $dst)) {
	if ($self->conf->slapt__get->enable) {
	    $self->add_requires($dst);
	}
	push @{$self->{_result}{output_files}}, $dst;
    } else {
	$self->error("move($src, $dst): $!");
	$self->errno(E_FAIL);
    }
}

sub slapt_get_required {
    my ($self) = @_;
    my $req = $self->request;
    my $conf = $self->conf->slapt__get->as_hash;

    if (my $p = $self->prereq) {
	my %h;
	$h{$req->package} = 1;
	[map {
	    my $packname = ref($_) eq 'HASH' ? $_->{package} : $_;
	    if ($h{$packname}) {
		()
	    } else {
		$h{$packname} = 1;
		if ($conf->{versions} ne 'never'
		    && ref($_) eq 'HASH' && (my $v = $_->{version})) {
		    if ($v =~ s/^([<=>]=?)([^\s].+)/$1 $2/) {
			;
		    } elsif ($v !~ /^[<=>]/) {
			$v = '>= '. $v;
		    }
		    $packname . ' ' . $v
		} elsif ($conf->{versions} eq 'always'
			 && (my @matches = $self->registry->lookup($packname))) {
		    $matches[0]->package . ' >= ' . $matches[0]->version
	        } else {
		    $packname
		}
	    }
	} @$p];
    }
}

sub add_requires {
    my ($self, $file) = @_;

    my $required = $self->slapt_get_required;

    return unless $required && @$required;
    $self->logger->info("Adding dependency information to $file");

    my $arc = new SlackBuild::Archive::Tar($file, append => 1);

    if ($arc->has_file('install/slack-required') &&
	!$self->conf->slapt__get->force) {
	return;
    }

    unless ($arc->add_file_content('install/slack-required',
				   join("\n", @$required) . "\n")
	    && $arc->save) {
	$self->errno(E_FAIL);
    }
}

sub os_probe {
    my $self = shift;

    unless (exists($self->{_os_release})) {
	my @args = ( 'docker',
		     'run',
		     '--rm=true',
		     $self->image,
		     '/bin/sh',
		     '-c',
		     'uname -m; cat /etc/os-release' );

	my $rc = new SlackBuild::Runcap(argv => \@args);
	unless ($rc->success) {
	    return $self->errno(E_EXEC);
	}
	$rc->rewind(SD_STDOUT);
	chomp($self->{_arch} = $rc->next_line(SD_STDOUT));
	while (my $s = $rc->next_line(SD_STDOUT)) {
	    chomp($s);
	    my ($name,$value)=split /=/, $s, 2;
	    $value =~ s/^"(.*)"\s*$/$1/;
	    $self->{_os}{$name} = $value;
	}

	if (my $name = $self->os_release('NAME')) {
	    if ($name ne 'Slackware') {
		$self->error('image '
			     . $self->image
			     . " is not a Slackware image (reported name: $name)");
		return $self->errno(E_FAIL);
	    }
	} else {
	    $self->error("can't determine distribution name");
	    return $self->errno(E_FAIL);
	}
	unless ($self->os_release('VERSION')) {
	    $self->error("can't determine Slackware version");
	    return $self->errno(E_FAIL);
	}
    }
    return E_OK;
}

sub os_release {
    my ($self, $value) = @_;
    return $self->{_os}{$value};
}

sub osversion {
    my $self = shift;
    my $version = $self->os_release('VERSION');
    if (my $codename =  $self->os_release('VERSION_CODENAME')) {
	$version .= '-' . $codename;
    }
    return $version
}

sub output_files {
    my $self = shift;
    $self->{_result}{output_files} //= [];
    if (wantarray) {
	return () if $self->errno;
	return (@{$self->{_result}{output_files}});
    } else {
	return @{$self->{_result}{output_files}};
    }
}

sub wd {
    my $self = shift;
    unless ($self->{_wd}) {
	$self->{_wd} = tempdir(DIR => $self->spooldir, CLEANUP => 1);
    }
    return $self->{_wd};
}

sub slackbuild_name {
    my $self = shift;
    return $self->package_name . '.SlackBuild';
}

sub run {
    my ($self, $req) = @_;

    $self->clear;

    if (!ref($req)) {
	my $name = $req;
	$req = try {
	    new SlackBuild::Request($name)
	} catch {
	    my $err = (split /\n/)[0];
	    $err =~ s{\s+at .* line \d+\.$}{};
	    $self->error("$name: $err");
	    undef
        };
	return $self->errno(E_SYNTAX) unless $req;
    }
    
    unless ($req->package) {
	$self->error("package: not present in request");
    }
    unless ($req->slackbuild_uri) {
	$self->error("slackbuild_uri: not present in request");
    }
    if ($self->errors) {
	return $self->errno(E_SYNTAX);
    }
    
    $self->request($req);

    my $archive = new SlackBuild::Archive($self->package_name,
					  $self->slackbuild_uri);

    return $self->errno(E_FAIL)
	unless $archive->download($self->wd);

    $self->os_probe;
    return $self->errno if $self->errno;
    
    $self->request->addinfo($archive->info->for_arch($self->arch));

    if (my $urilist = $self->request->source_uri) {
	my @md5sums = @{$self->request->md5sum // []};
	foreach my $s (@$urilist) {
	    my $uri = new SlackBuild::URI($s);
	    my $dest = File::Spec->catfile($self->wd,
			  $self->request->extract_local_name($uri->as_string));
	    return $self->errno(E_FAIL)
		unless $uri->download($dest);
	    if (my $checksum = shift @md5sums) {
		$self->logger->info("verifying $dest");
		$self->logger->debug("$dest: reported digest $checksum");
		if (open(my $fh, '<', $dest)) {
		    binmode($fh);
		    my $ctx = Digest::MD5->new;
		    my $digest = try {
			$ctx->addfile($fh);
		        $ctx->hexdigest;
		    } catch {
			$self->error($_);
			undef
		    };
		    close($fh);
		    return $self->errno(E_FAIL) unless $digest;
		    $self->logger->debug("$dest: computed digest $digest");
		    if ($digest ne $checksum) {
			$self->error("$dest fails checksum test");
		        return $self->errno(E_FAIL);
		    }
		} else {
		    $self->logger->error("$dest: checksum verification failed");
	            return $self->errno(E_FAIL);
	        }
	    }
	}
    }

    return $self->_build;
}  

sub _prepare {
    my $self = shift;
    
    # Find prerequisites
    # FIXME: Build a dependency graph and spawn slackbuilders for missing ones

    # FIXME: registry type should be configurable
    my $reg = new SlackBuild::Registry('FS', dir => $self->ospkgdir);
    $self->{_registry} = $reg;
    
    my $filename = File::Spec->catfile($self->wd, 'rc');
    return unless SlackBuild::Rc->new($self)->write($reg, $filename);
    return 'rc';
}

# Regex created using the following command:
# (find / -type d -not -name '/' -mindepth 1 -maxdepth 1 -printf '%f\n';
#  echo '.';
#  echo 'install') | xargs regexp-opt --pcre
my $file_name_rx = q{(?:b(?:in|oot)|dev|etc|home|install|l(?:ib(?:64)?|ost\+found)|m(?:edia|nt)|opt|proc|r(?:oot|un)|s(?:bin|rv|ys)|tmp|(?:us|va)r|\.)(?:/\w+)*};

# Parser states:
use constant {
    ST_INIT     => 0,  # Initial state
                       # [expect OUT]  "Creating Slackware package: foobar"
                       #         goto ST_CREATING
    ST_CREATING => 1,  # [expect OUT]  Empty line
                       #         goto ST_LISTING
                       # [else]  goto ST_BUILT
    ST_LISTING  => 2,  # [expect ERR]  File name (matching $file_name_rx)
                       #         remain in this state
                       # [else]  keep state
                       # [expect OUT]  Empty line
                       #         goto ST_CREATED
                       # [else]  goto ST_BUILT
    ST_CREATED  => 3,  # [expect OUT]  "Slackware package foo created"
	               #         goto ST_BUILT
    ST_BUILT    => 4,  # Essentially same as ST_INIT, except that we
                       # know that the package has been built
};

# state([$newstate])
# With argument - switch to that state.
# Return current state.
sub state {
    my ($self, $newstate) = @_;
    if (defined($newstate)) {
	$self->logger->debug("going to $newstate");
	$self->{_state} = $newstate;
    }
    return $self->{_state};
}

# Parsers for each particular state.
# Each parser is named by the corresponding state, converted to lower case,
# prefixed with underscore and suffixed by the channel name ('_err' or '_out').
# Argument is the line of text obtained from that channel, without trailing
# linefeed character.

sub _st_init_out {
    my ($self, $input) = @_;
    if ($input =~ m{^Creating Slackware package:}) {
	$self->logger->info($input);
	$self->state(ST_CREATING);
    }
}

sub _st_default_err {
    my ($self, $input) = @_;
    $self->errors->more('build');
}

sub _st_built_err {
    my ($self, $input) = @_;
    $self->errors->more('packaging');
}

sub _st_created_out {
    my ($self, $input) = @_;
    if ($input =~ m{^Slackware package /tmp/(.+?) created}) {
	$self->file($1);
	$self->logger->info($input);
	$self->state(ST_BUILT);
    } else {
        $self->logger->error("unexpected output: $input");
	$self->state(ST_INIT);
    }
}

sub _st_creating_out {
    my ($self, $input) = @_;
    if ($input eq '') {
	$self->state(ST_LISTING);
    } else {
	$self->state(ST_BUILT);
    }
}

sub _st_listing_err {
    my ($self, $input) = @_;
    if ($input =~ m{$file_name_rx}) {
	$self->logger->debug("adding file $input");
    } else {
	$self->logger->error($input);
    }
}

sub _st_listing_out {
    my ($self, $input) = @_;
    if ($input eq '') {
	$self->state(ST_CREATED);
    }
}    

# Parser tables bring together parsers for each channel:

my @parser_out = (
    \&_st_init_out,
    \&_st_creating_out,
    \&_st_listing_out,
    \&_st_created_out,
    \&_st_init_out
);

my @parser_err = (
    \&_st_default_err,
    \&_st_default_err,
    \&_st_listing_err,
    \&_st_default_err,
    \&_st_built_err
);    

# Finally, parser functions to be called for each channel:

sub parser_out {
    my ($self, $line) = @_;
    $self->logger->debug("OUT: $line");
    $self->${\$parser_out[$self->state]}($line);
}

sub parser_err {
    my ($self, $line) = @_;
    $self->logger->debug("ERR: $line");
    $self->${\$parser_err[$self->state]}($line);
}

sub commit_log {
    my ($self, $logfd) = @_;
    my $init;
    my @v = map {
	my $r = SlackBuild::Registry::Record->split($_);
	if ($r && (!$init || $r->package eq $self->package_name)) {
	    $init = 1;
	    $r
	} else {
	    ()
        }
    } $self->output_files;
    my $r;
    if (@v) {
	$r = pop(@v);
    } else {
	$r = SlackBuild::Registry::Record->new(
	            $self->package_name,
	            version => $self->package_version || 'UNKNOWN',
	            build => $self->package_build,
	            arch => $self->arch);
    }
    my $logname = File::Spec->catfile($self->logdir, $r . '.log');
    unless (rename $logfd->filename, $logname) {
	$self->logger->error("can't rename "
			     . $logfd->filename
			     . " to "
			     . $logname
			     . ": $!");
	$logname = $logfd->filename;
    }
    chmod 0666 & ~ umask(), $logname
	or $self->logger->error("can't change file mode of $logname: $!");
    $self->logger->info("Log file: $logname");
}    

sub _build {
    my $self = shift;
    
    my $contname = $self->package_name . '_slackbuild';
    my $hostname = $self->package_name . '.slackbuild.local';
    my $rcfile = $self->_prepare or return $self->errno(E_FAIL);
    my @opt;
    if ($self->request->tty) {
	# Need a tty.  Example: perl-Term-ReadLine-Gnu
	push @opt, '-t';
    }
    if ($self->conf->docker->unconfined || $self->request->unconfined) {
	# Need to dump executable image. Example: emacs
        push @opt, '--security-opt=seccomp=unconfined';
    }
    my @args = ( 'docker',
		 'run',
		 '--rm=true',
                 '--hostname='.$hostname,
                 '--add-host='.$hostname.':127.0.0.1',
		 '--workdir=/usr/src',
		 @opt,
		 '-v', $self->wd . ':/usr/src',
		 '-v', $self->tmpdir . ':/tmp',
		 '-v', $self->ospkgdir . ':/var/pkg:ro',
		 $self->image,
		 '/bin/sh',
		 $rcfile,
		 $self->slackbuild_name);

    $self->logger->info(
	sprintf("building %s on %s version %s (%s)\n",
		$self->package_name,
		$self->os_release('NAME'),
		$self->osversion,
	        $self->arch));

    my $logfd = try {
	File::Temp->new(UNLINK => 0,
			TEMPLATE => $self->package_name . ".XXXXXX",
			DIR => $self->logdir,
			SUFFIX => '.log');
    } catch {
	$self->logger->fatal("can't create log file: ". (split /\n/)[0]);
	undef;
    };
    return $self->errno(E_FAIL) unless $logfd;

    $self->state(ST_INIT);
    my $rc = new SlackBuild::Runcap(
	argv => \@args,
	stdout => sub {
	    (my $line = shift) =~ s/\r$//;
	    print $logfd "OUT: $line";
	    $logfd->flush();
	    chomp($line);
	    $self->parser_out($line);
	},
	stderr => sub {
	    (my $line = shift) =~ s/\r$//;
	    print $logfd "ERR: $line";
	    $logfd->flush();
	    chomp($line);
	    $self->parser_err($line);
	});
    $self->{_result}{docker} = $rc;
    unless ($rc->success) {
	$self->commit_log($logfd);
	return $self->errno(E_EXEC);
    }

    $self->commit_log($logfd);
    
    return $self->errno(E_OK);
}    

1;
