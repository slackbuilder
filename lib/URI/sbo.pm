# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

package URI::sbo;
use parent 'URI::_generic';
use File::Spec::Unix;

sub branch {
    my $self = shift;
    return $self->authority || 'HEAD';
}

sub host { shift->branch }

sub path {
    my $self = shift;
    my $ret = $self->SUPER::path(@_);
    $ret =~ s{^/}{};
    return $ret;
}

sub _pathcomp {
    my ($self, $n) = @_;
    # The array layout is:
    #   ($category, $package, @path)
    return (File::Spec::Unix->splitdir($self->path))[$n];
}

sub category { shift->_pathcomp(0) }
sub package { shift->_pathcomp(1) }    

1;
__END__

=head1 NAME

URI::sbo - slackbuilds URI

=head1 DESCRIPTION

The B<sbo> URIs refer to the packages in the L<git://git.slackbuilds.org/slackbuilds.git> repository. The format of the URI is:

    sbo://TREEISH/PATH

where TREEISH is the git tree identifier, and PATH is the pathname within the
repository. For example, the following refers to the latest version of
B<mailutils> package.

    sbo://HEAD/system/mailutils

=cut
