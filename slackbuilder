#!/bin/sh
#! -*- perl -*-
eval 'exec perl -x -wS $0 ${1+"$@"}'
    if 0;
# This file is part of slackbuilder
# Copyright (C) 2017-2021 Sergey Poznyakoff
#
# Slackbuilder is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Slackbuilder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with slackbuilder.  If not, see <http://www.gnu.org/licenses/>. */

use strict;
use warnings;
use Data::Dumper;
use Pod::Usage;
use Pod::Man;
use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use File::Basename;
use File::Spec;
use Unix::Sysexits;
use SlackBuilder;
use Net::SBo;
use JSON;

use constant {
    EX_FAIL => 1
};

my $progname = basename($0);
my $progdescr = "Slackware package builder";

sub abend {
    my $code = shift;
    print STDERR "$progname: " if defined($progname);
    print STDERR "@_\n";
    exit $code;
}

my %sbargs;
GetOptions("h" => sub {
               pod2usage(-message => "$progname: $progdescr",
			 -exitstatus => EX_OK);
	   },
	   "help" => sub {
	       pod2usage(-exitstatus => EX_OK, -verbose => 2);
	   },
	   "usage" => sub {
	       pod2usage(-exitstatus => EX_OK, -verbose => 0);
	   },
	   "verbose|v+" => \$sbargs{verbose},
	   "image|i=s" => \$sbargs{image},
    ) or exit(EX_USAGE);

abend(EX_USAGE, "bad number of arguments") unless @ARGV == 1;
my $builder = new SlackBuilder(%sbargs);
$builder->run($ARGV[0]);
if ($builder->errors) {
    $builder->logger->warn("there are messages in the following categories: ".join(', ', $builder->errors->categories));
}
if ($builder->is_success) {
    if ($builder->output_files == 0) {
	print STDERR "Build exited successfully, but no output files were generated\n";
	exit EX_FAIL;
    }
    print "OK. File list:\n";
    foreach my $f ($builder->output_files) {
	print "$f\n";
    }
    exit(EX_OK);
} else {
    exit($builder->errno);
}
